package projecto;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


class Personagem{
    List <Personagem> personagem;
    List<String> nome;
    List<Integer> id;
    List<Integer> tipo;
    List<Integer> orientacao;
    List<Integer> OrientacaoInicial;
    Mapa mapa;
    int x;
    int y;
    int[][]posicao = new int[x][y];
    String nomeTipo;
    String nomeOrientacao;
    
    public Personagem() {
        this.nome = new ArrayList();
        this.id = new ArrayList();
        this.tipo = new ArrayList();
        this.orientacao = new ArrayList();
    }
     
    public void orientarNorte(int x)
    {
        orientacao.set(x,0);
    }
    public void orientarEste(int x)
    {
        orientacao.set(x,2);
    }
    public void orientarSul(int x)
    {
       orientacao.set(x,4);
    }
     public void orientarOeste(int x)
    {
        orientacao.set(x,6);
    }
     
    public void moveNorte(int x){
        if(y == mapa.mapa.size() && y == mapa.numeroColunas){
            y=y+1;
        }
        else{
            orientarEste(x);
        }
    }
        
    public void moveEste(int x){
        if(x == mapa.mapa.size() && x == mapa.numeroColunas){
            x = x+1;  
        }
            
        else{
            orientarSul(x);
        }    
    }
        
    public void moveOeste(int x){
        if(x == mapa.mapa.size() && x == mapa.numeroColunas){
            x = x-1;  
        }
        else{
            orientarNorte(x);
        }
    }
        
    public void moveSul(int x){
        if(y == mapa.mapa.size() && y == mapa.numeroColunas){
            y = y-1;  
        }
        else{
             orientarOeste(x);
        }
    }  
    
    public String nomeOrientacao(int x){
        if (orientacao.get(x) == 0 ){
            nomeOrientacao = "Norte";  
        }
        if (orientacao.get(x) == 2){
            nomeOrientacao = "Este";  
        }
        if (orientacao.get(x) == 4){
            nomeOrientacao = "Sul";  
        }
        if (orientacao.get(x) == 6){
            nomeOrientacao = "Oeste";  
        }
        
       return nomeOrientacao;
    }
    
    public String nomeTipo(int x) {
        
        //usar switch
        
        if (tipo.get(x) == 0){
            nomeTipo = "Anao";  
        }
        if (tipo.get(x) == 1){
            nomeTipo = "Dragão";  
        }
        if (tipo.get(x) == 2){
            nomeTipo = "Elfo";  
        }
        if (tipo.get(x) == 3){
            nomeTipo = "Gigante";  
        }
        if (tipo.get(x) == 4){
            nomeTipo = "Humano";  
        }
       return nomeTipo;
    }

}

class Mapa{
    List<String> posicaoInicial;
    List<String> posicaoFinal;
    List <String> mapa;
    List <String> mapaFinal;
    int numeroColunas;
    String caracter;
    int x;
    int y;
    int[][] multi = new int[x][y];  
    
    public Mapa(){
        this.mapa = new ArrayList<>();
        this.posicaoInicial=new ArrayList<>();
    }
    
    public Mapa(int x,int y){
        this.x = x;
        this.y = y;
    }
    
    
   public void getChar(){
    char result = 0;
    for(x = 0; x < mapa.size(); x++){
        for(y = 0; y < numeroColunas;y++){
            result = mapa.get(x).charAt(y);
            if(result == 'V'){
                continue;
        }else {
                     posicaoInicial.add(x+","+result);    
            }
            }
        }      
   } 
}

public class Projecto {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Personagem p1 = new Personagem();
        
        //Le ficheiro personagens.txt
        try (BufferedReader br = new BufferedReader(new FileReader("personagem.txt")))
            {
                String line;
                while ((line = br.readLine()) != null) {       
                    String[] info = line.split(":");
                    p1.id.add(Integer.parseInt(info[0]));
                    p1.nome.add(info[1]);
                    p1.tipo.add(Integer.parseInt(info[2]));
                    p1.orientacao.add(Integer.parseInt(info[3])); 
                 }
            } catch (IOException e) {
                System.out.println("Ficheiro nao encontrado");
            } 
 
        
        //Le ficheiro mapa.txt
        Scanner leitorFicheiro = new Scanner(new FileReader("mapa.txt"));
        Mapa mapa = new Mapa();
        
        while (leitorFicheiro.hasNextLine()) {
            String line = leitorFicheiro.nextLine();
            mapa.numeroColunas = line.length();
            mapa.mapa.add(line);
        }
        
        mapa.mapaFinal = new ArrayList(mapa.mapa);// copia de mapa original para modificar
        mapa.getChar(); // dá coordenadas ocupadas no mapa
        mapa.posicaoFinal = new ArrayList(mapa.posicaoInicial);
      
        //modifica Orientação
        p1.OrientacaoInicial = new ArrayList(p1.orientacao);
        p1.orientarEste(0);
        p1.orientarNorte(1);
        p1.orientarSul(2);
        p1.orientarNorte(3);
        
      //Escreve em resultados.txt 
       try{
            PrintWriter writer = new PrintWriter("resultados.txt");
            writer.println("Estado Inicial");
            for(int x = 0; x < mapa.mapa.size(); x++){
                writer.println(mapa.mapa.get(x));
            }
            writer.write(System.getProperty( "line.separator" ));
            writer.println("Estado Final\n");
            for(int x = 0; x < mapa.mapaFinal.size(); x++){
                writer.println(mapa.mapaFinal.get(x));
            }
            writer.write(System.getProperty( "line.separator" ));
            
            writer.println("Personagens");
            for(int x = 0; x < p1.nome.size(); x++){
                writer.println(p1.nome.get(x) + " (" + p1.nomeTipo(x) + ")" );
                writer.println(mapa.posicaoInicial.get(x)+ "(" + p1.OrientacaoInicial.get(x)+")");
                writer.println(mapa.posicaoFinal.get(x)+ "(" + p1.nomeOrientacao(x) + ")");
                writer.write(System.getProperty( "line.separator" ));  
            }
            
            writer.close();
        }
        catch (IOException e) {
            System.out.println("Problema ao escrever no ficheiro");
        }       
    }
}
    

