package projecto;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;


class Personagem{
    List <Personagem> personagem;
    List<String> nome;
    List<Integer> id;
    List<Integer> tipo;
    List<Integer> orientacao;

    public Personagem() {
        this.nome = new ArrayList();
        this.id = new ArrayList();
        this.tipo = new ArrayList();
        this.orientacao = new ArrayList();
    }
 
}

class Mapa {
    List<String> posicaoInicial;
    List<String> posicaoFinal;
    List <String> mapa;
    List <String> mapaFinal;
    List<Integer> tipoDePersonagem;
    List<Integer> orientacaoNoMapa;
    List<Integer> pontos;
    List<Integer> premioOuro;
    List<Integer> premioPrata;
    List<Integer> premioBronze;
    int numeroColunas;
    int x;
    int y;
    
    public Mapa(){
        this.mapa = new ArrayList<>();
        this.posicaoInicial = new ArrayList<>();
        this.pontos = new ArrayList<>(Collections.nCopies(10, 0)); //o collections serve para prencher a list com 0 de modo a não
        this.premioOuro = new ArrayList<>(Collections.nCopies(10, 0));//ficar null
        this.premioPrata = new ArrayList<>(Collections.nCopies(10, 0));
        this.premioBronze = new ArrayList<>(Collections.nCopies(10, 0));
    }
    
    public Mapa(int x,int y){
        this.x = x;
        this.y = y;
    }
    
    
   public void getChar(char h){ //encontra personagens no mapa e guarda a sua posição numa list
    char result = 0;
    for(x = 0; x < mapa.size(); x++){
        for(y = 0; y < numeroColunas;y++){
            result = mapa.get(x).charAt(y);
            if(result == h){
                posicaoInicial.add(x+","+y);    
        }else {
                     continue;  
            }
            }
        }  
   }
   
   public void mudaPosicaoMapa(int coordenada_x, int coordenada_y,int h){
       String alteraPersonagem = mapaFinal.get(coordenada_x);
       StringBuilder sb = new StringBuilder(); // reconstroi String colocando personagem na nova posição
       char[] buff = alteraPersonagem.toCharArray();
       sb.append(buff , 0, coordenada_y).append(h);
       sb.append(buff,coordenada_y+1,buff.length -(coordenada_y+1));
       mapaFinal.set(coordenada_x,sb.toString());
   }
   
    public void apagaPosicaoMapa(int coordenada_x, int coordenada_y,int h){
       
       String alteraPersonagem = mapaFinal.get(coordenada_x);
       StringBuilder sb = new StringBuilder();  // reconstroi String apagando o personagem do mapa
       char[] buff = alteraPersonagem.toCharArray();
       if(h==3){
           sb.append(buff , 0, coordenada_y).append('Z');
           sb.append(buff,coordenada_y+1,buff.length -(coordenada_y+1));
        }else{
            sb.append(buff , 0, coordenada_y).append('V');
            sb.append(buff,coordenada_y+1,buff.length -(coordenada_y+1));
       }
       mapaFinal.set(coordenada_x,sb.toString());
   }
    
    public void premios(char ocupacao,int h){
        
         switch(ocupacao){
            case 'O':
                premioOuro.set(h,+1);
                pontos.set(h,+3);
                break;
            case 'P':
                premioPrata.set(h,+1);
                pontos.set(h,+2);
                break;
            case 'B':
                premioBronze.set(h,+1);
                pontos.set(h,+1);
                break;
        }    
    }
    
    public void mudaOrientacao(int tipo , int h ,int coordenada_x, int coordenada_y){
         switch(tipo){
            case 0: case 3:
                orientacaoNoMapa.set(h,orientacaoNoMapa.get(h)+2);
                if(orientacaoNoMapa.get(h)>7)
                    orientacaoNoMapa.set(h,0);    
                break;
                            
            case 1: case 2: case 4:
                 orientacaoNoMapa.set(h,orientacaoNoMapa.get(h)+1);
                 if(orientacaoNoMapa.get(h)>7)
                    orientacaoNoMapa.set(h,0);    
             
                 break;
        }
    }
    
   
    public void movePersonagem(int h){
      
        String resultado;
        int coordenada_x=0;
        int casas =0;
        char fixPosition;
        int coordenada_y;
       
        fixPosition = posicaoFinal.get(h).charAt(2);// vai buscar posição no y
        coordenada_y= fixPosition -'0';
        resultado = posicaoFinal.get(h);
        coordenada_x = resultado.charAt(0) - '0'; // vai buscar a posição x
        int LimiteY = numeroColunas -1;
        int LimiteX= mapa.size()-1;
 
            switch(tipoDePersonagem.get(h)){
                case 0:        //Anao
                    casas=1;
                    break;
                case 1:case 3: //Dragão e Gigante
                    casas=3;
                    break;
                case 2:case 4: //Elfo e Humano
                    casas=2;
                    break;  
            }
   
            switch(orientacaoNoMapa.get(h)){
            
                case 0: //Norte
                    int TestOffLimitsXnorte= coordenada_x-casas;
                    char OcupacaoNorte=0;
                    if(TestOffLimitsXnorte>=0&&TestOffLimitsXnorte<=LimiteX){
                    OcupacaoNorte= mapa.get(TestOffLimitsXnorte).charAt(coordenada_y);
                    }
                    //Testa Limites do Mapa
                    if(TestOffLimitsXnorte<0||TestOffLimitsXnorte>LimiteX||OcupacaoNorte=='Z'||
                            OcupacaoNorte=='1'||OcupacaoNorte=='2'||OcupacaoNorte=='3'||OcupacaoNorte=='4'||
                            OcupacaoNorte=='5'||OcupacaoNorte=='6'||OcupacaoNorte=='7'||OcupacaoNorte=='8'||
                            OcupacaoNorte=='9'){
                    
                    mudaOrientacao(tipoDePersonagem.get(h),h,coordenada_x,coordenada_y);
                        
                    }else{
                        switch(tipoDePersonagem.get(h)){//chama função para apagar personagem do mapa caso seja um gigante
                                                         //irá deixar um buraco no mapa por isso é utilizado um switch
                            
                            case 0: case 1: case 2: case 4:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                            case 3:
                                apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                        }
                        coordenada_x=coordenada_x-casas;
                        
                        //Actualiza posicao no mapa
                        mudaPosicaoMapa(coordenada_x,coordenada_y,h);
                        premios(OcupacaoNorte,h); 
                        break;
                    }
                case 1: //Nordeste
                    int TestOffLimitsXnordeste= coordenada_x-casas;
                    int TestOffLimitsYnordeste= coordenada_y+casas;
                    char OcupacaoNordeste=0;
                    
                    if(TestOffLimitsXnordeste>=0&&TestOffLimitsYnordeste>=0&&TestOffLimitsYnordeste>=LimiteY&&
                            TestOffLimitsXnordeste>=LimiteX){
                    OcupacaoNordeste= mapa.get(TestOffLimitsXnordeste).charAt(TestOffLimitsYnordeste);
                }
                   //Testa Limites do Mapa
                    if(TestOffLimitsXnordeste<0 || TestOffLimitsXnordeste>LimiteX || TestOffLimitsYnordeste<0 || 
                            TestOffLimitsYnordeste>LimiteY ||OcupacaoNordeste=='Z'||OcupacaoNordeste=='0'||OcupacaoNordeste=='1'||
                            OcupacaoNordeste=='2'||OcupacaoNordeste=='3'||OcupacaoNordeste=='4'||
                            OcupacaoNordeste=='5'||OcupacaoNordeste=='6'||OcupacaoNordeste=='7'||
                            OcupacaoNordeste=='8'||OcupacaoNordeste=='9'){
                       
                       mudaOrientacao(tipoDePersonagem.get(h),h,coordenada_x,coordenada_y);
                        
                    }else{
                       
                        switch(tipoDePersonagem.get(h)){
                            
                            case 0: case 1: case 2: case 4:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                            case 3:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                        }
                        coordenada_x=coordenada_x-casas;
                        coordenada_y=coordenada_y+casas;
                        
                        //Actualiza posicao no mapa
                        mudaPosicaoMapa(coordenada_x,coordenada_y,h);
                        premios(OcupacaoNordeste,h);
                        
                    }
                break;
                
                case 2://Este
                    int TestOffLimitsYeste= coordenada_y+casas;
                    char OcupacaoEste=0;
                    
                    if(TestOffLimitsYeste>=0&&TestOffLimitsYeste<=LimiteY){
                    OcupacaoEste= mapa.get(coordenada_x).charAt(TestOffLimitsYeste);
                    }
                    //Testar Limites do Mapa
                    if(TestOffLimitsYeste<0||TestOffLimitsYeste>LimiteY || OcupacaoEste=='Z'||OcupacaoEste=='0'||
                         OcupacaoEste=='1'||OcupacaoEste=='2'||OcupacaoEste=='3'||OcupacaoEste=='4'||
                            OcupacaoEste=='5'||OcupacaoEste=='6'||OcupacaoEste=='7'||OcupacaoEste=='8'||
                            OcupacaoEste=='9'){
 
                        mudaOrientacao(tipoDePersonagem.get(h),h,coordenada_x,coordenada_y);
                        
                    }else{
                        switch(tipoDePersonagem.get(h)){
                            
                            case 0: case 1: case 2: case 4:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                            case 3:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                        }
                        coordenada_y=coordenada_y+casas;
                        
                        //Actualiza posicao no mapa
                        mudaPosicaoMapa(coordenada_x,coordenada_y,h);
                        premios(OcupacaoEste,h);  
                    }
                    break;
                case 3://Sudeste
                    int TestOffLimitsXsudeste= coordenada_x+casas;
                    int TestOffLimitsYsudeste= coordenada_y+casas;
                    char OcupacaoSudeste=0;
                    
                    if(TestOffLimitsXsudeste>=0 && TestOffLimitsYsudeste>=0 && TestOffLimitsXsudeste<=LimiteX&&
                            TestOffLimitsYsudeste<=LimiteY){
                    OcupacaoSudeste= mapa.get(TestOffLimitsXsudeste).charAt(TestOffLimitsYsudeste);
                    }
                    //Testa Limites do Mapa
                    if(TestOffLimitsXsudeste<0 || TestOffLimitsXsudeste>LimiteX || TestOffLimitsYsudeste<0 ||
                            TestOffLimitsYsudeste>LimiteY || OcupacaoSudeste=='Z'|| OcupacaoSudeste=='0'||
                            OcupacaoSudeste=='1'||OcupacaoSudeste=='2'||OcupacaoSudeste=='3'||OcupacaoSudeste=='4'||
                            OcupacaoSudeste=='5'||OcupacaoSudeste=='6'||OcupacaoSudeste=='7'||OcupacaoSudeste=='8'||
                            OcupacaoSudeste=='9'){
              
                        mudaOrientacao(tipoDePersonagem.get(h),h,coordenada_x,coordenada_y);
                        
                    }else{
                        switch(tipoDePersonagem.get(h)){
                            
                            case 0: case 1: case 2: case 4:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                            case 3:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                        }
                        coordenada_x=coordenada_x+casas;
                        coordenada_y=coordenada_y+casas;
                        
                        //altera posicao no mapa                       
                        mudaPosicaoMapa(coordenada_x,coordenada_y,h);
                        premios(OcupacaoSudeste,h);
    
                    }
                break;
                case 4://Sul
                    int TestOffLimitsXsul= coordenada_x+casas;
                    char OcupacaoSul=0;
                    if(TestOffLimitsXsul>=0 && TestOffLimitsXsul<=LimiteX){
                        OcupacaoSul= mapa.get(TestOffLimitsXsul).charAt(coordenada_y);
                    }
                    //Testa Limites do Mapa
                    if(TestOffLimitsXsul<0||TestOffLimitsXsul>LimiteX || OcupacaoSul=='Z'||OcupacaoSul=='0'||
                            OcupacaoSul=='1'||OcupacaoSul=='2'||OcupacaoSul=='3'||OcupacaoSul=='4'||
                            OcupacaoSul=='5'||OcupacaoSul=='6'||OcupacaoSul=='7'||OcupacaoSul=='8'||
                            OcupacaoSul=='9'){
                        
                        mudaOrientacao(tipoDePersonagem.get(h),h,coordenada_x,coordenada_y);
                        
                    }else{
                        switch(tipoDePersonagem.get(h)){ 
                            
                            case 0: case 1: case 2: case 4:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                            case 3:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                        }
                        coordenada_x=coordenada_x+casas;  
                        //altera personagem no mapa
                        mudaPosicaoMapa(coordenada_x,coordenada_y,h);
                        premios(OcupacaoSul,h);
                        
                    }
                break;
                case 5://Sudoeste
                    int TestOffLimitsXsudoeste= coordenada_x+casas;
                    int TestOffLimitsYsudoeste= coordenada_y-casas;
                    char OcupacaoSudoeste=0;
                    //Testa Limites do Mapa
                    if(TestOffLimitsXsudoeste>=0 && TestOffLimitsYsudoeste >=0 && TestOffLimitsYsudoeste<=LimiteY &&
                            TestOffLimitsXsudoeste<=LimiteX){
                        OcupacaoSudoeste= mapa.get(TestOffLimitsXsudoeste).charAt(TestOffLimitsYsudoeste);
                    }
  
                    if(TestOffLimitsXsudoeste<0 || TestOffLimitsXsudoeste>LimiteX || TestOffLimitsYsudoeste<0 ||
                            TestOffLimitsYsudoeste>LimiteY ||OcupacaoSudoeste=='Z'|| OcupacaoSudoeste=='0'|| OcupacaoSudoeste=='1'||
                            OcupacaoSudoeste=='2'||OcupacaoSudoeste=='3'||OcupacaoSudoeste=='4'||
                            OcupacaoSudoeste=='5'||OcupacaoSudoeste=='6'||OcupacaoSudoeste=='7'||
                            OcupacaoSudoeste=='8'||OcupacaoSudoeste=='9'){
                    
                         mudaOrientacao(tipoDePersonagem.get(h),h,coordenada_x,coordenada_y);
                        
                    }else{                   
                        switch(tipoDePersonagem.get(h)){
                            
                            case 0: case 1: case 2: case 4:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                            case 3:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                        }
                        coordenada_x=coordenada_x+casas;
                        coordenada_y=coordenada_y-casas;
                        
                        //altera posicao no mapa
                        mudaPosicaoMapa(coordenada_x,coordenada_y,h);  
                        premios(OcupacaoSudoeste,h);
                    }
                break;
                case 6://Oeste
                    int TestOffLimitsYoeste= coordenada_y-casas;
                    char OcupacaoOeste=0;
                    //Testa Limites do Mapa
                    if(TestOffLimitsYoeste>=0){
                        OcupacaoOeste= mapa.get(coordenada_x).charAt(TestOffLimitsYoeste);
                    }
             
                    if(TestOffLimitsYoeste<0||TestOffLimitsYoeste>LimiteY||OcupacaoOeste=='Z'||OcupacaoOeste=='0'||
                            OcupacaoOeste=='1'||OcupacaoOeste=='2'||OcupacaoOeste=='3'||OcupacaoOeste=='4'||
                            OcupacaoOeste=='5'||OcupacaoOeste=='6'||OcupacaoOeste=='7'||OcupacaoOeste=='8'||
                            OcupacaoOeste=='9'){
                       mudaOrientacao(tipoDePersonagem.get(h),h,coordenada_x,coordenada_y);
           
                    }else{
                        switch(tipoDePersonagem.get(h)){
                            case 0: case 1: case 2: case 4:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                            case 3:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                        }
                        coordenada_y=coordenada_y-casas;
                        
                        //altera posicao no mapa
                        mudaPosicaoMapa(coordenada_x,coordenada_y,h);
                        premios(OcupacaoOeste,h);
                       
                    }
                break;
                case 7://Noroeste
                    int TestOffLimitsXnoroeste= coordenada_x-casas;
                    int TestOffLimitsYnoroeste= coordenada_y-casas;
                    char OcupacaoNoroeste=0;
                    
                    if(TestOffLimitsXnoroeste>=0&&TestOffLimitsYnoroeste>=0){
                         OcupacaoNoroeste= mapa.get(TestOffLimitsXnoroeste).charAt(TestOffLimitsYnoroeste);
                    }
                    //Testa limites no mapa
                    if(TestOffLimitsXnoroeste<0 || TestOffLimitsXnoroeste>LimiteX || TestOffLimitsYnoroeste<0 ||
                            TestOffLimitsYnoroeste>LimiteY ||OcupacaoNoroeste=='Z'|| OcupacaoNoroeste=='0'||OcupacaoNoroeste=='1'||
                            OcupacaoNoroeste=='2'||OcupacaoNoroeste=='3'||OcupacaoNoroeste=='4'||
                            OcupacaoNoroeste=='5'||OcupacaoNoroeste=='6'||OcupacaoNoroeste=='7'||OcupacaoNoroeste=='8'||
                            OcupacaoNoroeste=='9'){
               
                        mudaOrientacao(tipoDePersonagem.get(h),h,coordenada_x,coordenada_y);
                        
                    }else{    
                        switch(tipoDePersonagem.get(h)){
                            
                            case 0: case 1: case 2: case 4:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                            case 3:
                                 apagaPosicaoMapa(coordenada_x,coordenada_y,tipoDePersonagem.get(h));
                                 break;
                        }
                        coordenada_x=coordenada_x-casas;
                        coordenada_y=coordenada_y-casas;
                        
                        //altera posicao no mapa
                        mudaPosicaoMapa(coordenada_x,coordenada_y,h);
                        premios(OcupacaoNoroeste,h);
                    }
                break;
            }
      
            posicaoFinal.set(h,coordenada_x+","+coordenada_y);// modifica list
        
    }
     
}

public class Projecto {
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Personagem p1 = new Personagem();
        
        //Le ficheiro personagens.txt
        try (BufferedReader br = new BufferedReader(new FileReader("personagem.txt")))
            {
                String line;
                while ((line = br.readLine()) != null) {       
                    String[] info = line.split(":");
                    p1.id.add(Integer.parseInt(info[0]));
                    p1.nome.add(info[1]);
                    p1.tipo.add(Integer.parseInt(info[2]));
                    p1.orientacao.add(Integer.parseInt(info[3])); 
                 }
            } catch (IOException e) {
                System.out.println("Ficheiro nao encontrado");
            } 
 
        
        //Le ficheiro mapa.txt
        Mapa mapa = new Mapa();
        try{
            Scanner leitorFicheiro = new Scanner(new FileReader("mapa.txt"));
            while (leitorFicheiro.hasNextLine()) { 
               String line = leitorFicheiro.nextLine();
               mapa.numeroColunas = line.length();
               mapa.mapa.add(line);
            }     
        }catch(FileNotFoundException e) {
            System.out.println("Ficheiro nao encontrado");
             }
       
         
        mapa.getChar('0'); // dá as coordenadas de cada personagem
        mapa.getChar('1');
        mapa.getChar('2');
        mapa.getChar('3');
        mapa.getChar('4');
        mapa.getChar('5');
        mapa.getChar('6');
        mapa.getChar('7');
        mapa.getChar('8');
        mapa.getChar('9');
        
        mapa.mapaFinal = new ArrayList(mapa.mapa);// copia de mapa original para modificar
        mapa.posicaoFinal = new ArrayList(mapa.posicaoInicial);
      
        mapa.tipoDePersonagem = p1.tipo;//copia do tipo para puder modificar no mapa
        mapa.orientacaoNoMapa = p1.orientacao;//copia da orientacao para puder modificar no mapa
 
      //Escreve em resultados.txt 
       try{
            PrintWriter writer = new PrintWriter("resultados.txt");
            writer.println("Estado Inicial");
            for(int x = 0; x < mapa.mapa.size(); x++){
                writer.println(mapa.mapa.get(x));
            }
            writer.write(System.getProperty( "line.separator" ));
           
            for(int x=0;x<5;x++){
                
                for(int f=0; f<p1.id.size();f++){
                    mapa.movePersonagem(f);
                    }
                writer.println("Turno "+(x+1)+":");
                for(int l = 0; l < mapa.mapaFinal.size(); l++){
                    writer.println(mapa.mapaFinal.get(l));
                }   
                writer.write(System.getProperty( "line.separator" ));  
            }
              
            writer.println("Tesouros");
            for(int x = 0; x <p1.nome.size(); x++){
                writer.println(p1.nome.get(x)+ ":" + mapa.pontos.get(x)+ ":" + mapa.premioOuro.get(x) +
                         ":" + mapa.premioPrata.get(x) +  ":" + mapa.premioBronze.get(x));
            }
            writer.close();
        }
        catch (IOException e) {
            System.out.println("Problema ao escrever no ficheiro");
        }
    }
    
}
    

