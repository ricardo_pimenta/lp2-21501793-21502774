
package projecto;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


public class MapaTest {
    
    public MapaTest() {
    }
    
    @Before
    public void setUp() {
    }

  
    @Test
    public void testMovePersonagemAnao() {
        Mapa mapa2 = new Mapa();
        Personagem p2 = new Personagem();
        p2.orientacao.add(0,2);
        p2.tipo.add(0,0);
        mapa2.mapa.add(0,"0VVV");
        mapa2.mapa.add(1,"VVVV");
        mapa2.mapaFinal = new ArrayList(mapa2.mapa);
        mapa2.numeroColunas =4;
        mapa2.getChar('0');
        mapa2.posicaoFinal = new ArrayList(mapa2.posicaoInicial);
        mapa2.tipoDePersonagem = p2.tipo;
        mapa2.orientacaoNoMapa = p2.orientacao;
        String posicaoEsperada = "0"+","+"1";
        mapa2.movePersonagem(0);
        assertEquals(posicaoEsperada, mapa2.posicaoFinal.get(0)); 
        
    }
    
     @Test
    public void testMovePersonagemDragao() {
        Mapa mapa2 = new Mapa();
        Personagem p2 = new Personagem();
        p2.orientacao.add(0,2);
        p2.tipo.add(0,1);
        mapa2.mapa.add(0,"0VVV");
        mapa2.mapa.add(1,"VVVV");
        mapa2.mapaFinal = new ArrayList(mapa2.mapa);
        mapa2.numeroColunas =4;
        mapa2.getChar('0');
        mapa2.posicaoFinal = new ArrayList(mapa2.posicaoInicial);
        mapa2.tipoDePersonagem = p2.tipo;
        mapa2.orientacaoNoMapa = p2.orientacao;
        String posicaoEsperada = "0"+","+"3";
        mapa2.movePersonagem(0);
        assertEquals(posicaoEsperada, mapa2.posicaoFinal.get(0)); 
    }
    
    @Test 
    public void testMovePersonagemElfo() {
        Mapa mapa2 = new Mapa();
        Personagem p2 = new Personagem();
        p2.orientacao.add(0,2);
        p2.tipo.add(0,2);
        mapa2.mapa.add(0,"0VVV");
        mapa2.mapa.add(1,"VVVV");
        mapa2.mapaFinal = new ArrayList(mapa2.mapa);
        mapa2.numeroColunas =4;
        mapa2.getChar('0');
        mapa2.posicaoFinal = new ArrayList(mapa2.posicaoInicial);
        mapa2.tipoDePersonagem = p2.tipo;
        mapa2.orientacaoNoMapa = p2.orientacao;
        String posicaoEsperada = "0"+","+"2";
        mapa2.movePersonagem(0);
        assertEquals(posicaoEsperada, mapa2.posicaoFinal.get(0)); 
    }
    
    @Test
    public void testMovePersonagemGigante() {
        Mapa mapa2 = new Mapa();
        Personagem p2 = new Personagem();
        p2.orientacao.add(0,2);
        p2.tipo.add(0,3);
        mapa2.mapa.add(0,"0VVV");
        mapa2.mapa.add(1,"VVVV");
        mapa2.mapaFinal = new ArrayList(mapa2.mapa);
        mapa2.numeroColunas =4;
        mapa2.getChar('0');
        mapa2.posicaoFinal = new ArrayList(mapa2.posicaoInicial);
        mapa2.tipoDePersonagem = p2.tipo;
        mapa2.orientacaoNoMapa = p2.orientacao;
        String posicaoEsperada = "0"+","+"3";
        mapa2.movePersonagem(0);
        assertEquals(posicaoEsperada, mapa2.posicaoFinal.get(0)); 
    }
    
     @Test
     public void testMovePersonagemHumano() {
        Mapa mapa2 = new Mapa();
        Personagem p2 = new Personagem();
        p2.orientacao.add(0,2);
        p2.tipo.add(0,3);
        mapa2.mapa.add(0,"0VVV");
        mapa2.mapa.add(1,"VVVV");
        mapa2.mapaFinal = new ArrayList(mapa2.mapa);
        mapa2.numeroColunas =4;
        mapa2.getChar('0');
        mapa2.posicaoFinal = new ArrayList(mapa2.posicaoInicial);
        mapa2.tipoDePersonagem = p2.tipo;
        mapa2.orientacaoNoMapa = p2.orientacao;
        String posicaoEsperada = "0"+","+"3";
        mapa2.movePersonagem(0);
        assertEquals(posicaoEsperada, mapa2.posicaoFinal.get(0)); 
    }
     
     @Test
     public void testMovePersonagemTurno() {
        Mapa mapa2 = new Mapa();
        Personagem p2 = new Personagem();
        
        p2.orientacao.add(0,2);
        p2.orientacao.add(1,2);
        p2.tipo.add(0,3);//Gigante
        p2.tipo.add(1,0);//Anao
        mapa2.mapa.add(0,"0VVV");//Gigante posicionado em 0,0
        mapa2.mapa.add(1,"VVVV");
        mapa2.mapa.add(2,"1VVV"); //Anao posicionado em 2,0
        mapa2.mapaFinal = new ArrayList(mapa2.mapa);
        mapa2.numeroColunas =4;
        mapa2.getChar('0');//vai buscar jogador 0
        mapa2.getChar('1');//vai buscar jogador 1
        mapa2.posicaoFinal = new ArrayList(mapa2.posicaoInicial);
        mapa2.tipoDePersonagem = p2.tipo;
        mapa2.orientacaoNoMapa = p2.orientacao;
        String posicaoEsperada1 = "0"+","+"3";
        String posicaoEsperada2 = "2"+","+"1";
        for(int f=0; f<p2.tipo.size();f++){
            mapa2.movePersonagem(f);
        }
        assertEquals(posicaoEsperada1, mapa2.posicaoFinal.get(0)); 
        assertEquals(posicaoEsperada2, mapa2.posicaoFinal.get(1));
    }
}
